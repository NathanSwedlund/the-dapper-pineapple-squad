import java.sql.*;
import com.mysql.cj.jdbc.*;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class LoginCheck
 */
@WebServlet("/LoginCheck")
public class LoginCheck extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginCheck() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// Because the method = "post" in the first form of the "SimpleLogin.jsp", submit will come here.
		String username = request.getParameter("username");
		String password = request.getParameter("password");
					
		
		Connection connect = null;
		Statement statement = null;
		PreparedStatement preparedStatement = null;
		ResultSet resultSet = null;
		try 
		{
			// This will load the MySQL driver, each DB has its own driver
			Class.forName("com.mysql.cj.jdbc.Driver");
			
			// Setup the connection with the DB
 			connect = DriverManager.getConnection("jdbc:mysql://root@localhost/passwordTest?" + "user=Nathan&password=blee");
			// Statements allow to issue SQL queries to the database
			statement = connect.createStatement();
			statement.executeQuery("use peer_eval");
			
			resultSet = statement.executeQuery("select * from templogin");
			boolean isMember = false;
			
			while(resultSet.next() && !isMember)
			{	
				boolean isProfessor = resultSet.getBoolean("IsProfessor");
				if(username.equals(resultSet.getString("username")) && password.equals(resultSet.getString("password")))
				{
					isMember = true;
					if(isProfessor)
						response.sendRedirect("Teacher.jsp");
					else 
						response.sendRedirect("Student.jsp");
				}
			}
			if(!isMember)
			{
				System.out.print("failed login");
				response.sendRedirect("Login.jsp");

			}

		} catch (Exception e) {
			//throw e;
		} finally {
			try {
				if (resultSet != null) {
					resultSet.close();
				}

				if (statement != null) {
					statement.close();
				}

				if (connect != null) {
					connect.close();
				}
			} catch (Exception e) {
			}
		}
	}
}
